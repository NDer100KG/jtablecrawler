import sqlite3
import re, os
import requests
from bs4 import BeautifulSoup
import time
import pandas as pd
import logging
from tqdm import tqdm

FORMAT = "%(asctime)s %(levelname)s: %(message)s"
logging.basicConfig(level=logging.INFO, format=FORMAT)


class Jtable_crawler(object):
    def __init__(self) -> None:
        super().__init__()
        self.subscribe_file = "subscribes.csv"
        self.db_name = "jtable.db"
        self.token_txt = "line_token.txt"
        self.subscribes = []
        self.new_videos = {}
        self.send_line_notify_gate = True

    def get_subscribes(self):
        data = pd.read_csv(self.subscribe_file, header=None)
        data = data.values
        self.subscribes = [d[0] for d in data]
        logging.info("subscribes " + str(len(self.subscribes)) + " actresses")

    def unsubscribe(self):
        for s in self.saved_actress:
            logging.info("Unsubscribe " + s)
            self.cursor.execute(""" DROP TABLE """ + s)

    def crawl_items(self, url):
        r = requests.get(url)
        if r.status_code == 200:
            html = r.text
            soup = BeautifulSoup(html, "html.parser")
            actress_name = soup.findAll("h2", {"class": "h3-md mb-1"})[0].contents[0]
            logging.info("get actress " + actress_name)
            if actress_name in self.saved_actress:
                self.saved_actress.remove(actress_name)
            else:
                logging.info("Create table: " + actress_name)
                self.create_table(
                    "CREATE TABLE IF NOT EXISTS "
                    + actress_name
                    + "("
                    + "name text(255),"
                    + "code text(255),"
                    + "link text(255),"
                    + "image image"
                    + ");"
                )

            all_items = soup.findAll("div", {"class": "video-img-box mb-e-20"})
            for item in all_items:
                try:
                    video_name = item.contents[3].contents[1].text.split(" ")[1]
                    if not self.check_if_item_exists(actress_name, video_name):
                        link = item.contents[3].contents[1].contents[0]["href"]
                        self.push_item(
                            actress_name,
                            {
                                "name": video_name,
                                "code": item.contents[3].contents[1].text.split(" ")[0],
                                "link": link,
                                "image": item.contents[1].contents[1].contents[1]["data-src"],
                            },
                        )
                        self.new_videos[video_name] = link
                except:
                    logging.error(item.contents[3].contents[1].text)

    def push_item(self, table, item):
        sql_command = """ INSERT INTO """ + table + """ VALUES(?,?,?,?) """
        self.cursor.execute(sql_command, (item["name"], item["code"], item["link"], item["image"]))

    def check_if_item_exists(self, table, item_name):
        sql_command = (
            """ SELECT * FROM  """ + table + """ WHERE name =  \"""" + item_name + """\" """
        )
        rows = self.cursor.execute(sql_command).fetchall()
        if len(rows) == 0:
            return False
        else:
            return True

    def create_table(self, create_table_sql):
        try:
            self.cursor.execute(create_table_sql)
        except:
            logging.error("Create table fail")

    def save_db(self):
        self.db.commit()


    def create_db(self):
        if not os.path.exists(self.db_name):
            self.send_line_notify_gate = False
        self.db = sqlite3.connect(self.db_name)
        self.cursor = self.db.cursor()
        self.get_subscribes()
        self.saved_actress = self.cursor.execute(
            """ SELECT name FROM sqlite_master WHERE type = "table" """
        ).fetchall()
        self.saved_actress = [i[0] for i in self.saved_actress] ## convert to list without other signs

        for url in self.subscribes:
            self.crawl_items(url)

        if len(self.saved_actress) > 0:
            self.unsubscribe()

        if self.send_line_notify_gate:
            self.send_line_notify()
        self.save_db()

    def close(self):
        self.cursor.close()
        self.db.close()
        
    def send_line_notify(self):
        f = open(self.token_txt, 'r')
        headers = {
            "Authorization": "Bearer " + f.readline(),
            "Content-Type": "application/x-www-form-urlencoded",
        }

        if len(self.new_videos) > 0:
            msg = ""
            for k, v in self.new_videos.items(): 
                msg = msg + k + '\n' + v + '\n'
            params = {"message": msg}
        else:
            params = {'message': "No new upload today"}

        r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=params)
        if r.status_code == 200:
            logging.info("send successfully")
        else:
            logging.error()


if __name__ == "__main__":
    crawler = Jtable_crawler()
    crawler.create_db()
    crawler.close()